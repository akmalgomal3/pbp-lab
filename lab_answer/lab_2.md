1. Perbedaan antara JSON dan XML adalah
JSON (JavaScript Object Notation)
    Adalah format pertukaran data yang fleksibel berbasis teks dengan menggunakan tipe data teks dan angka untuk merepresentasikan objek. Hal Ini adalah format standar  dari bahasa pemrograman JavaScript.
    JSON lebih cocok untuk pertukaran data antara aplikasi web dan request web. Sebagai bahasa markup, XML hanya menambahkan informasi tambahan ke teks biasa, sedangkan JSON, seperti namanya, adalah cara mewakili objek data.

XML (Extensive Markup Language)
    Adalah format data berbasis teks yang berasal dari SGML dan ditulis dengan cara yang sama diikuti oleh HTML. Format XML sudah ada selama bertahun-tahun dan telah dikembangkan.
    Keuntungan utama XML adalah platform yang independen. hal ini berarti pengguna dapat mengambil data dari program lain seperti SQL dan mengubahnya menjadi XML kemudian membagikan data dengan platform lain. XML adalah teknologi berorientasi dokumen yang menyediakan kemampuan untuk menyimpan dan menampilkan data dalam format yang dapat dibaca mesin dan yang dapat dibaca manusia.

Perbedaan
Bahasa :
    XML merupakan bahasa markup dan bukanlah bahasa pemrograman yang memiliki tag untuk mendefinisikan elemen.
    JSON hanyalah format yang ditulis dalam JavaScript

Kecepatan :
    XML sangat besar dan lambat dalam penguraian, menyebabkan transmisi data lebih lambat.
    JSON sangat cepat karena ukuran file sangat kecil dan penguraian lebih cepat oleh mesin JavaScript

Penggunaan Array :
    XML tidak mensupport array secara langsung, jika ingin menggunakan array harus menggunakan tag.
    JSON Mensupport Array.

2. Perbedaan HTML dan XML adalah
    XML sangat Case Sensitive, sedangkan HTML tidak peka dengan Case Sensitive atau besar dan kecil tiap huruf dan kalimatnya.
    XML sangat ketat dalam memperhatikan tag penutup, sedangkan HTML tidak terlalu ketat pada tag penutup.
    Tag XML biasanya dapat dikembangkan kembali, sedangkan HTML memiliki tag yang sedikit terbatas.
    HTML banyak digunakan untuk membuat tampilan untuk halaman website dan banyak hal mengenai cara-cara menampilkan sebuah informasi atau data, sedangkan XML disini lebih banyak berperan sebagai transport data yang digunakan membawa data agar dapat digunakan untuk berpindah platform menjadi lebih mudah.
