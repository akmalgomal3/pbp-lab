import '../flutter_flow/flutter_flow_theme.dart';
import '../flutter_flow/flutter_flow_util.dart';
import '../flutter_flow/flutter_flow_widgets.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePageWidget extends StatefulWidget {
  HomePageWidget({Key key}) : super(key: key);

  @override
  _HomePageWidgetState createState() => _HomePageWidgetState();
}

class _HomePageWidgetState extends State<HomePageWidget> {
  bool _loadingButton1 = false;
  bool _loadingButton2 = false;
  bool _loadingButton3 = false;
  bool _loadingButton4 = false;
  bool _loadingButton5 = false;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Color(0xFFD1D1D1),
        iconTheme: IconThemeData(color: Color(0xFF009688)),
        automaticallyImplyLeading: true,
        title: Align(
          alignment: AlignmentDirectional(-0.45, 0),
          child: Image.asset(
            'assets/images/Logo_Hachoo.png',
            width: MediaQuery.of(context).size.width * 0.5,
            height: 50,
            fit: BoxFit.contain,
          ),
        ),
        actions: [],
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      drawer: Drawer(
        elevation: 16,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: EdgeInsetsDirectional.fromSTEB(0, 20, 0, 0),
              child: FFButtonWidget(
                onPressed: () {
                  print('Button pressed ...');
                },
                text: 'Home Page',
                options: FFButtonOptions(
                  width: 200,
                  height: 40,
                  color: FlutterFlowTheme.primaryColor,
                  textStyle: FlutterFlowTheme.subtitle2.override(
                    fontFamily: 'Poppins',
                    color: Colors.white,
                  ),
                  borderSide: BorderSide(
                    color: Colors.white,
                    width: 1,
                  ),
                  borderRadius: 12,
                ),
                loading: _loadingButton1,
              ),
            ),
            Divider(),
            FFButtonWidget(
              onPressed: () {
                print('Button pressed ...');
              },
              text: 'Vaksin',
              options: FFButtonOptions(
                width: 200,
                height: 40,
                color: FlutterFlowTheme.primaryColor,
                textStyle: FlutterFlowTheme.subtitle2.override(
                  fontFamily: 'Poppins',
                  color: Colors.white,
                ),
                borderSide: BorderSide(
                  color: Colors.transparent,
                  width: 1,
                ),
                borderRadius: 12,
              ),
              loading: _loadingButton2,
            ),
            Divider(),
            FFButtonWidget(
              onPressed: () {
                print('Button pressed ...');
              },
              text: 'Informasi',
              options: FFButtonOptions(
                width: 200,
                height: 40,
                color: FlutterFlowTheme.primaryColor,
                textStyle: FlutterFlowTheme.subtitle2.override(
                  fontFamily: 'Poppins',
                  color: Colors.white,
                ),
                borderSide: BorderSide(
                  color: Colors.transparent,
                  width: 1,
                ),
                borderRadius: 12,
              ),
              loading: _loadingButton3,
            ),
            Divider(),
            FFButtonWidget(
              onPressed: () {
                print('Button pressed ...');
              },
              text: 'Layanan Pengguna',
              options: FFButtonOptions(
                width: 200,
                height: 40,
                color: FlutterFlowTheme.primaryColor,
                textStyle: FlutterFlowTheme.subtitle2.override(
                  fontFamily: 'Poppins',
                  color: Colors.white,
                ),
                borderSide: BorderSide(
                  color: Colors.transparent,
                  width: 1,
                ),
                borderRadius: 12,
              ),
              loading: _loadingButton4,
            ),
            Divider(),
            FFButtonWidget(
              onPressed: () {
                print('Button pressed ...');
              },
              text: 'Donasi',
              options: FFButtonOptions(
                width: 200,
                height: 40,
                color: FlutterFlowTheme.primaryColor,
                textStyle: FlutterFlowTheme.subtitle2.override(
                  fontFamily: 'Poppins',
                  color: Colors.white,
                ),
                borderSide: BorderSide(
                  color: Colors.transparent,
                  width: 1,
                ),
                borderRadius: 12,
              ),
              loading: _loadingButton5,
            ),
            Divider()
          ],
        ),
      ),
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Align(
              alignment: AlignmentDirectional(0, -1),
              child: Padding(
                padding: EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
                child: Text(
                  'Hachoo adalah website dengan bantuan herokuapp yang memiliki tujuan untuk\nmengurangi penyebaran Covid-19 di daerah DKI Jakarta. Hachoo sendiri telah menjadi\nlayanan informasi penyedia vaksin di wilayah DKI Jakarta yang dinilai efektif untuk\ndigunakan.',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1,
                ),
              ),
            ),
            Align(
              alignment: AlignmentDirectional(0, -0.5),
              child: Image.asset(
                'assets/images/Atas.png',
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.5,
                fit: BoxFit.cover,
              ),
            )
          ],
        ),
      ),
    );
  }
}
